% rebase('base.html')

<ul>
    <div class="row">
        % import interface
        % for post in posts:
            <div class="flowtow">
                <li class="image col-sm-4">
                    <img src="/static/images/{{post['filename']}}" alt="{{post['filename'][:-4]}}">
                    <div class="details">
                        <p class='user'>{{post['user']}}</p>
                        <p class='date'>{{post['timestamp']}}</p>
                        <div class="likes">
                            <form role="form" action="/like" method="post">
                                <input type="hidden" name="filename" value="{{post['filename']}}">
                                <span>{{post['likes']}}</span>
                                <input type="submit" class="btn btn-primary" value="Like">
                            </form>
                        </div>
                    </div>
                </li>
            </div>
        % end
    </div>
</ul>
