'''
@author: Ronald Peic 42115132
'''


from bottle import Bottle, template, debug, static_file, redirect, request, response
import interface
import users
from database import COMP249Db
import os


COOKIE_NAME = 'sessionid'
UPLOAD_DIR = 'static/images'
NUM_IMG_DISP = 3

application = Bottle()


@application.route('/')
def index():
    db = COMP249Db()
    num_img_displayed = 3
    data = {
        'title': 'FlowTow',
        'heading': "Welcome back " + users.session_user(db) if users.session_user(db) else 'Welcome to FlowTow!',
        'subtitle': 'Hover over images for more information',
        'posts': interface.list_images(db, num_img_displayed),
        'user': users.session_user(db)
    }
    return template('index', data)


@application.route('/my')
def my_images():
    """The logged in user's page which only displays the user's images
    Otherwise, return the login page for user that is not yet logged in
    """
    db = COMP249Db()
    if not users.session_user(db):
        # data = {
        #     'title': 'Login',
        #     'heading': 'Log in to FlowTow!',
        #     'subtitle': 'Please log in first to view your posts',
        #     'user': users.session_user(db)
        # }
        # return template('index', data)
        redirect('/')
    data = {
        'title': 'FlowTow',
        'heading': 'My FlowTow',
        'subtitle': 'Hover over images for more information',
        'posts': interface.list_images(db, NUM_IMG_DISP, users.session_user(db)),
        'user': users.session_user(db)
    }
    return template('my_images', data)


@application.route('/about')
def about():
    db = COMP249Db()
    data = {
        'title': 'About',
        'heading': 'About FlowTow!',
        'subtitle': '',
        'user': users.session_user(db)
    }
    return template('about', data)


@application.route('/<user>')
def user_page(user):
    """Displays the queried user's posts"""
    db = COMP249Db()
    data = {
        'title': 'FlowTow',
        'heading': user + '\'s FlowTow',
        'subtitle': 'Hover over images for more information',
        'posts': interface.list_images(db, NUM_IMG_DISP, user),
        'user': users.session_user(db)
    }
    return template('index', data)


@application.route('/loginform')
def login_form():
    """Page with login form which is only accessible if not yet logged in"""
    db = COMP249Db()
    users.delete_session(db, '*')
    data = {
        'title': 'Login',
        'heading': 'Log in to FlowTow!',
        'subtitle': '',
        'user': users.session_user(db)
    }
    return template('login', data)


@application.post('/login')
def login_user():
    """Method for processing login form and generating user session"""
    db = COMP249Db()
    username = request.forms['nick']
    password = request.forms['password']
    if not users.check_login(db, username, password):
        data = {
            'title': 'Login',
            'heading': 'Login Failed, please try again',
            'subtitle': '',
            'user': users.session_user(db)
        }
        return template('failed_login', data)
    users.generate_session(db, username)
    redirect('/')


@application.route('/logout')
def logout():
    """Removes the user session from the database then redirects user to index page"""
    db = COMP249Db()
    username = users.session_user(db)
    if username:
        users.delete_session(db, username)
    response.set_cookie(COOKIE_NAME, '', expires=0)
    redirect('/')


@application.post('/like')
def likes():
    """Get the likes for the image, increment the number of likes, then return the refreshed page with updated likes"""
    db = COMP249Db()
    image = request.forms.get('filename')
    # headers = request.headers
    # referer = headers['Referer'].split('8080/')
    # username = users.session_user(db)
    # if username:
    #     interface.add_like(db, image, username)
    # else:
    #     interface.add_like(db, image)
    # redirect('/' + referer[1])
    interface.add_like(db, image)
    return redirect('/')


@application.post('/upload')
def upload():
    """Handle file upload form"""
    # if user is not logged in, redirect to index
    db = COMP249Db()
    if not users.session_user(db):
        return redirect('/')

    # get the 'newfile' field from the form
    imagefile = request.files.get('imagefile')

    # only allow upload of image files
    if imagefile.content_type != 'image/jpeg':
        print('something something something')
        # data = {
        #     'title': 'FlowTow',
        #     'heading': 'Upload failed',
        #     'subtitle': 'Only images can be uploaded',
        #     'posts': interface.list_images(db, NUM_IMG_DISP, users.session_user(db)),
        #     'user': users.session_user(db)
        # }
        # return template('my_images', data)
        return redirect('/my')

    save_path = os.path.join(UPLOAD_DIR, imagefile.filename)
    imagefile.save(save_path)

    # submit image upload details to the database
    print("adding an image to the database")
    interface.add_image(db, imagefile.filename, users.session_user(db))

    # redirect to home page if it all works ok
    return redirect('/my')


@application.route('/static/<filename:path>')
def static(filename):
    return static_file(filename=filename, root='static')


if __name__ == '__main__':
    debug()
    application.run()
