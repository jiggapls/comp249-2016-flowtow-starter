'''
@author: Ronald Peic 42115132
'''
import uuid
import bottle
from database import COMP249Db
db = COMP249Db()

# this variable MUST be used as the name for the cookie used by this application
COOKIE_NAME = 'sessionid'


def check_login(db, usernick, password):
    """returns True if password matches stored"""
    cur = db.cursor()
    if usernick and password:
        cur.execute('SELECT password FROM users WHERE nick = ?', (usernick,))
        storedpw = cur.fetchone()
        if storedpw and storedpw[0] == db.encode(password):
            return True
    else:
        return False


def generate_session(db, usernick):
    """create a new session and add a cookie to the response object (bottle.response)
    user must be a valid user in the database, if not, return None
    There should only be one session per user at any time, if there
    is already a session active, use the existing sessionid in the cookie
    """
    cur = db.cursor()
    cur.execute("SELECT nick FROM users WHERE nick = ?", (usernick,))
    user = cur.fetchall()
    # Check if the usernick exists in the database
    if user and usernick:
        key = bottle.request.get_cookie(COOKIE_NAME)
        cur.execute("SELECT sessionid FROM sessions WHERE usernick = ?", (usernick,))
        user_session = cur.fetchone()
        if user_session and not key:
            key = user_session[0]
            bottle.response.set_cookie(COOKIE_NAME, key)
        # If user session does not exist in the database (i.e. not logged in) then create one in the database
        if not user_session:
            key = str(uuid.uuid4())
            cur.execute("INSERT INTO sessions VALUES (?, ?)", (key, usernick))
            db.commit()
            bottle.response.set_cookie(COOKIE_NAME, key)
        return key
    else:
        return None


def delete_session(db, usernick):
    """remove all session table entries for this user"""
    cur = db.cursor()
    cur.execute('DELETE FROM sessions WHERE usernick = ?', (usernick,))
    db.commit()


def session_user(db):
    """try to retrieve the user from the sessions table
    return usernick or None if no valid session is present"""
    cur = db.cursor()
    key = bottle.request.get_cookie(COOKIE_NAME)
    cur.execute("SELECT usernick FROM sessions WHERE sessionid = ?", (key,))
    user = cur.fetchone()
    if user:
        return user[0]
    else:
        return None