'''
@author: Ronald Peic 42115132
'''

import datetime
from database import COMP249Db
db = COMP249Db()


def list_images(db, n, usernick=None):
    """Return a list of dictionaries for the first 'n' images in
    order of timestamp. Each dictionary will contain keys 'filename', 'timestamp', 'user' and 'likes'.
    The 'likes' value will be a count of the number of likes for this image as returned by count_likes.
    If usernick is given, then only images belonging to that user are returned."""
    cur = db.cursor()
    img_list = []
    if usernick:
        # Check if usernick exists in database
        cur.execute("SELECT nick FROM users WHERE nick = ?", (usernick,))
        username = cur.fetchone()
        if username:
            cur.execute("SELECT filename, timestamp, usernick FROM images WHERE usernick = ?", (usernick,))
    else:
        # If no usernick provided, return all images
        cur.execute("SELECT filename, timestamp, usernick FROM images")
    images = cur.fetchall()
    # Configure each post as a dictionary to be added to the list of images
    for image in images:
        post = {
            'filename': image[0],
            'timestamp': image[1],
            'user': image[2],
            'likes': count_likes(db, image[0])
        }
        img_list.append(post)
    # Sort the list of dictionaries
    img_list.sort(key=lambda k: k['timestamp'], reverse=True)
    # Slice number of posts to the specified parameter
    if len(img_list) > n:
        img_list = img_list[:n]
    return img_list


def add_image(db, filename, usernick):
    """Add this image to the database for the given user"""
    cur = db.cursor()
    # cur.execute("SELECT nick FROM users WHERE nick = ?", (usernick,))
    # username = cur.fetchone()
    # # Check if usernick exists in the database
    # if username:
    # cur.execute("INSERT INTO images (filename, usernick) VALUES (?, ?)", (filename, usernick))
    cur.execute("INSERT INTO images VALUES (?, ?, ?)", (filename, datetime.datetime.now(), usernick))
    img = cur.fetchall
    db.commit()


def add_like(db, filename, usernick=None):
    """Increment the like count for this image"""
    cur = db.cursor()
    cur.execute("SELECT filename FROM images WHERE filename = ?", (filename,))
    fn = cur.fetchone()
    cur.execute("SELECT nick FROM users WHERE nick = ?", (usernick,))
    username = cur.fetchone()
    # Check if filename exists in database
    if fn:
        # Check if usernick exists in database
        if usernick and username:
            cur.execute("INSERT INTO likes VALUES (?, ?)", (filename, usernick))
        elif not usernick:
            cur.execute("INSERT INTO likes VALUES (?, ?)", (filename, None))
        db.commit()


def count_likes(db, filename):
    """Count the number of likes for this filename"""
    cur = db.cursor()
    cur.execute("SELECT filename, usernick FROM likes WHERE filename = ?", (filename,))
    rows = cur.fetchall()
    num_likes = len(rows)
    return num_likes
